package net.nilosplace.interval_tree;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Main {
	
	public Main() throws Exception {
		File f = new File("ready/MGI2.out");
		
		BufferedReader reader = new BufferedReader(new FileReader(f));
		
		IntervalTree<Data> tree = new IntervalTree<Data>();
		
		String line = null;
		int count = 0;
		while((line = reader.readLine()) != null) {
			String[] array = line.split(" ");
			Data d = new Data();
			d.id = count++;
			tree.addInterval(new Interval<Data>(Long.parseLong(array[0]), Long.parseLong(array[1]), d));
			//System.out.println(array[0] + " " + array[1]);

		}
		
		tree.build();
		for(Interval<Data> i: tree.getIntervals(45, 55)) {
			System.out.println(i);
		}
		tree.print();
	}
	
	public static void main(String[] args) throws Exception {
		new Main();
	}
}
